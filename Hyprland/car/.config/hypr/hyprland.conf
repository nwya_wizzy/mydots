#MONITOR
monitor=,preferred,auto,auto

#AUTOSTART
 exec-once = waybar & hyprpaper
 exec-once systemctl --user restart xdg-desktop-portal.service

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

input {
    kb_layout = us,ru
    kb_variant =
    kb_model =
    kb_options = grp:alt_shift_toggle
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }
}

general {
    gaps_in = 5
    gaps_out = 20
    border_size = 0
    col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
    col.inactive_border = rgba(595959aa)

    layout = dwindle
}

decoration {
    rounding = 10
    blur = no
    blur_size = 3
    blur_passes = 1
    blur_new_optimizations = on
    drop_shadow = no
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
    dim_inactive = 1
    dim_strength = 0.6
}

animations {
    enabled= yes
    bezier=overshot,0.13,0.99,0.29,1.1
    animation=windows,1,4,overshot,slide
    animation=border,1,10,default
    animation=fade,1,10,default
    animation=workspaces,1,6,overshot,slidevert
}

dwindle {
    pseudotile = yes 
    preserve_split = yes # you probably want this
    force_split = no
}

master {
    new_is_master = no
}

gestures {
    workspace_swipe = off
}

misc {
      disable_hyprland_logo = true
      disable_splash_rendering = true
}

device:epic mouse V1 {
    sensitivity = -0.5
}
windowrule = float, file_progress
windowrule = float, confirm
windowrule = float, dialog
windowrule = float, download
windowrule = float, notification
windowrule = float, error
windowrule = float, splash
windowrule = float, confirmreset
windowrule = float, title:Open File
windowrule = float, title:branchdialog
windowrule = float, Lxappearance
windowrule = float, Rofi
windowrule = animation none,Rofi
windowrule = float,viewnior
windowrule = float,feh
windowrule = float, pavucontrol-qt
windowrule = float, pavucontrol
windowrule = float, file-roller
windowrulev2 = float,class:^(telegramdesktop)$
windowrule = move 1450 90,^(telegramdesktop)$
windowrule = size 420 900,^(telegramdesktop)$
windowrule=move center,title:^(fly_is_kitty)$
windowrule=size 800 500,title:^(fly_is_kitty)$
windowrule=float,title:^(fly_is_kitty)$
windowrulev2 = float,class:^(telegramdesktop)$
windowrule = move 1450 90,^(telegramdesktop)$
windowrule = size 420 900,^(telegramdesktop)$
#KEYS

$mainMod = SUPER

bind = $mainMod, RETURN, exec, kitty
bind = $mainMod, t, exec, kitty --title fly_is_kitty
bind = $mainMod, C, killactive, 
bind = $mainMod, M, exit, 
bind = $mainMod, E, exec, chromium
bind = $mainMod, V, togglefloating, 
bind = $mainMod, R, exec, rofi -show drun
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle
bind = SUPER_SHIFT, S, exec, systemctl suspend

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow
# Ресайз
bind = $mainMod CTRL, left, resizeactive,-50 0
bind = $mainMod CTRL, right, resizeactive,50 0
bind = $mainMod CTRL, up, resizeactive,0 -50
bind = $mainMod CTRL, down, resizeactive,0 50

