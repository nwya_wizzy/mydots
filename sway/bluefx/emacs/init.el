(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(eval-after-load 'gnutls '(add-to-list 'gnutls-trustfiles "/etc/ssl/cert.pem")) ;; axe from here (in v29)
(unless package-archive-contents (package-refresh-contents))
(unless (package-installed-p 'use-package) (package-install 'use-package))
(eval-when-compile (require 'use-package)) 
(require 'bind-key) ;; too here
(setq use-package-always-ensure t)

;;; modules ;;;

(use-package emacs               :config  (menu-bar-mode -1)
                                          (tool-bar-mode -1)
                                          (scroll-bar-mode -1)
                                          (horizontal-scroll-bar-mode -1)
                                          (savehist-mode 1)
                                 :custom  (inhibit-startup-message t)
                                          (display-line-numbers-type `relative)
                                          (native-comp-deferred-compilation nil)
                                          (dired-kill-when-opening-new-dired-buffer t)
                                          (indent-tabs-mode nil)
                                          (ubiquify-buffer-name-style 'forward)
                                          (visible-bell t)
                                          (user-full-name "stass")
                                          (user-mail-address "")
                                          (org-directory "~/doc/org"))

  (use-package dashboard           :preface (defun my/dashboard-banner ()
                                              (setq dashbard-banner-logo-title
                                                    (format "Hello Stanislaw"
                                                            (emacs-init-time) gcs-done)))
                                   :custom  (dashboard-startup-banner "~/.local/share/emacs/emacs.png")
                                            (initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
                                            (dashboard-center-content t)
                                            (dashboard-items '((recents  . 5) (bookmarks . 5)))
                                   :config  (dashboard-setup-startup-hook)
                                 :hook    ((after-init     . dashboard-refresh-buffer)
                                           (dashboard-mode . my/dashboard-banner)))

(defun create-dashboard-buffer nil
  "Create a dashboard buffer"
  (interactive)
  (switch-to-buffer (get-buffer-create "*dashboard*"))
  (lisp-interaction-mode))
(global-set-key (kbd "C-S-d") 'create-dashboard-buffer)

(set-face-attribute 'default nil :font "JetBrainsMono Nerd Font" :height 135)
(use-package doom-themes         :config  (load-theme 'doom-tomorrow-night t))
(use-package doom-modeline       :config  (doom-modeline-mode))

(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "multimarkdown"))

;;; go part ;;;

(use-package lsp-mode
  :defer t)

(defun ime-go-before-save ()
  (interactive)
  (when lsp-mode
    (lsp-organize-imports)
    (lsp-format-buffer)))

(use-package go-mode
  :defer t
  :config
  (add-hook 'go-mode-hook 'lsp-deferred)
  (add-hook 'go-mode-hook
            (lambda ()
              (add-hook 'before-save-hook 'ime-go-before-save))))


;; Display line numbers in every buffer
(global-display-line-numbers-mode 1)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("18c5ec0e4d1723dbeadb65d17112f077529fd24261cb8cd4ceee145e6a6f4cd1" default))
 '(package-selected-packages
   '(doom-themes go-mode modus-themes use-package lsp-mode doom-modeline dashboard)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
