(define-package "lsp-mode" "20230515.549" "LSP mode"
  '((emacs "26.3")
    (dash "2.18.0")
    (f "0.20.0")
    (ht "2.3")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0")
    (eldoc "1.11"))
  :commit "e92475a0cb9e53a7cddea153248fe4cf1dec4d8b" :authors
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainers
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  '("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :keywords
  '("languages")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
