###--Variables------------------------------------------------------
    set $mod Mod4
    set $left h
    set $down j
    set $up k
    set $right l
    set $term alacritty
    set $menu rofi -show drun
    set $lock swaylock --image ~/mydots/background/blue_nebo.jpg --clock --indicator --indicator-radius 100 --ring-color bde6fb


###--USER BINDS------------------------------------------------------
    bindsym --to-code $mod+b exec firefox
    bindsym --to-code $mod+n exec nemo
    bindsym --to-code $mod+d exec Discord
    bindsym --to-code $mod+t exec telegram-desktop


###--RULES-----------------------------------------------------
### WS
	assign [app_id="firefox"] workspace 1
        assign [app_id="org.telegram.desktop"] workspace 2
        assign [app_id="Emacs"] workspace 3
	assign [app_id="Steam"] workspace 5
	assign [app_id="nemo"] workspace 4
###FLOAT
	#for_window [app_id="org.telegram.desktop"] floating enable
	for_window [app_id="lutris"] floating enable
	for_window [app_id="feh"] floating enable
	for_window [app_id="mpv"] floating enable
        for_window [app_id="nemo"] floating enable
        #for_window [app_id="firefox"] floating enable
###--AUTOSTART------------------------------------------------------
    exec ~/.azotebg 
    exec dbus-run-session pipewire
    exec autotiling
    exec emacs --daemon
    exec mako


###--SWAYIDLE--------------------------------------------------------
   exec swayidle -w \
            timeout 600 'swaylock --image ~/mydots/background/blue_nebo.jpg --clock --indicator --indicator-radius 100 --ring-color bde6fb' \
            timeout 900 'systemctl suspend' \
            before-sleep 'swaylock --image ~/mydots/background/blue_nebo.jpg --clock --indicator --indicator-radius 100 --ring-color bde6fb'

###--- APPEARANCE ---------------------------------------------------
    default_border pixel 2

### GAPS
    gaps outer 18
    gaps inner 18

### CORNER RADIUS
    corner_radius 10

### BLUR
    blur enable
    blur_xray disable
    blur_passes 3
    blur_radius 3

### SHADOWS
    shadows on
    shadow_color #F9F9F97F
    shadow_blur_radius 10

### THEME
    set $gnome-schema org.gnome.desktop.interface
    exec_always {
            gsettings set $gnome-schema gtk-theme 'ZorinBlue-Dark'
            gsettings set $gnome-schema icon-theme 'Zorin'
            gsettings set $gnome-schema cursor-theme 'capitaine-cursors'
            gsettings set $gnome-schema font-name 'JetBrainsMonoNerdFont 10'
    }

### CURSORS
    seat seat0 xcursor_theme "capitaine-cursors"
    exec_always gsettings set org.gnome.desktop.interface cursor-theme "capitaine-cursors"

### WINDOW DECORATION
    client.focused			#f2f2f2 #f2f2f2 #f2f2f2 #f2f2f2
    client.focused_inactive		#272a34 #272a34 #272a34 #272a34
    client.unfocused		        #272a34 #272a34 #272a34 #272a34
    client.urgent			#272a34 #272a34 #272a34 #272a34
    client.placeholder		        #272a34 #272a34 #272a34 #272a34

###--KEYBOARD-------------------------------------------------------
        input * {
            xkb_layout us,ru
            xkb_options grp:caps_toggle
        }


###--XWAYLAND-------------------------------------------------------
        xwayland enable


###--Key bindings---------------------------------------------------
### BASE
    # TERMINAL
    bindsym $mod+Return exec $term

    # Lock screen
    bindsym $mod+Shift+w exec $lock

    # Kill focused window
    bindsym $mod+c kill

    # Start your launcher
    bindsym $mod+r exec $menu
    
    # Float mod
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+q reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+q exec swaynag -t warning -m 'НУ ЧЕ? УРОКИ ПОШЕЛ ДЕЛАТЬ' -B 'Yes, exit sway' 'swaymsg exit'

### MOVES

    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right

### WORKSPACES

    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6

### LAYOUT
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent

### SCRATCHPAD

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show

### RESIZE

mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+r mode "resize"


###--BAR---------------------------------------------------
bar {
	swaybar_command waybar
}


###--OTHER---------------------------------------------------
include /etc/sway/config.d/*
