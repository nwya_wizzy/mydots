# BSPWM RICE # 

![image](/bspwm/white/images/image1.png)
![image with fetch](/bspwm/white/images/image2.png)
![image dmenu](/bspwm/white/images/image3.png)
![image with dm-logout scrtpt](/bspwm/white/images/image4.png)

## Credits ##

+ [dochi77 (for some polybar modules)](https://gitlab.com/dochi77/arch-rices/)
+ [debxp (dm-logout scipt)](https://github.com/debxp/dmenu-scripts)
+ [prolinux (polybar modules + polybar style)](https://notabug.org/owl410/owl_dotfiles/)
