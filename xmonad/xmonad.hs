import XMonad

import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad

import Graphics.X11.ExtraTypes.XF86

import XMonad.ManageHook
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName

import XMonad.Layout.Maximize
import XMonad.Layout.Spacing
import XMonad.Layout.Renamed
import XMonad.Layout.ThreeColumns

import XMonad.Actions.Promote
import XMonad.Actions.CycleWS
import XMonad.Actions.CycleRecentWS
import XMonad.Actions.SinkAll

import Control.Monad (liftM2)
import qualified Data.Map        as M
import qualified XMonad.StackSet as W

main = xmonad . ewmhFullscreen . ewmh $ def {
	terminal = my_terminal,
	modMask = my_mod_mask,
	borderWidth = my_border_width,
	layoutHook = my_layouts,
	focusFollowsMouse = my_focus_follows_mouse,
	clickJustFocuses = my_click_just_focuses,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        workspaces         = myWorkspaces,
	startupHook = setWMName "LG3D",
	manageHook = namedScratchpadManageHook scratchpads
}
	`additionalKeysP` my_keys

myWorkspaces = ["1","2","3","4","5"]
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"
my_terminal = "kitty"
my_focus_follows_mouse = False
my_click_just_focuses = False
my_border_width = 1 
my_mod_mask = mod4Mask
my_lock_screen = "slock"
my_layouts = threeCol 
threeCol = spacing 15 
	$ ThreeColMid nmaster delta ratio
nmaster = 1
ratio = 1 / 2
delta = 3 / 100

-- scratchPads
scratchpads :: [NamedScratchpad]
scratchpads = [
    NS "term" "kitty --title scratchpad" (title =? "scratchpad") 
    (customFloating $ W.RationalRect (1/4) (1/4) (2/4) (2/4)),
    NS "ranger" "kitty --title ranger -e ranger" (title =? "ranger") 
    (customFloating $ W.RationalRect (1/4) (1/4) (2/4) (2/4)),
    NS "ncmpcpp" "kitty --title ncmpcpp -e ncmpcpp" (title =? "ncmpcpp") 
    (customFloating $ W.RationalRect (2/6) (2/6) (2/6) (2/6))
  ]

my_keys = [
	("M-e", spawn "chromium"),
	("M-c", kill),
	("M-d", spawn "rofi -show drun"),
	("M-f", withFocused $ sendMessage . maximizeRestore),
	("M--", promote),
	("M-Enter", spawn my_terminal),
	("M-t", namedScratchpadAction scratchpads "term"),
	("M-r", namedScratchpadAction scratchpads "ranger"),
	("M-n", namedScratchpadAction scratchpads "ncmpcpp"),
	("M-<Space>", toggleRecentNonEmptyWS),
	("M-[", sendMessage Shrink),
	("M-]", sendMessage Expand),
	("M-y", withFocused $ windows . W.sink),
	("M-'", decWindowSpacing 2),
	("M-;", incWindowSpacing 2),
	("M-m", spawn my_lock_screen),
	("<XF86AudioLowerVolume>", spawn "amixer -q set Master 5%- unmute"),
	("<XF86AudioRaiseVolume>", spawn "amixer -q set Master 5%+ unmute"),
	("<XF86AudioMute>", spawn "amixer -q set Master toggle"),
	("<Print>", spawn "flameshot gui")
	]



